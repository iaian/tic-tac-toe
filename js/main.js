$( document ).ready(function() {

	// focus first name input
	$('#crossesPlayerName').focus();

	// insure this input is focused onclick
	setTimeout(function() {
		$('#crossesPlayerName').focus();
	}, 200);

	// stop namesForm from submiting
	$('.namesForm').submit(function(event) {
		event.preventDefault();
	});

	// set player names button
	$('.setPlayerNamesBtn').click(function() {
		setPlayerNames();
	});

	// click on the TTT board
	$('.tttBoard div').click(function() {
		setCurrentPlayerMove($(this));

		// DEBUG
		/*
		console.log('playerOneScoreArray : ' + playerOneScoreArray);
		console.log('playerTwoScoreArray : ' + playerTwoScoreArray);
		*/
	});

	// reset button
	$("body").on("click", ".resetBtn", function() {
		window.location = '';
	});

	// rematch button
	$("body").on("click", ".rematchBtn", function() {
		readyTheBoard(0);
		$('.modal').modal('hide');
	});

});

/*-- GLOBAL VARS --*/

var playerOneName,
playerTwoName,
playerOneIconHtml = '<i class="icon-remove"></i>',
playerTwoIconHtml = '<i class="icon-circle-blank"></i>',
playerOneScoreArray = [],
playerTwoScoreArray = [],
currentPlayer,
currentPlayerIcon;

var winningMoves = [
	["1","2","3"],
	["4","5","6"],
	["7","8","9"],
	["1","5","9"],
	["3","5","7"],
	["1","4","7"],
	["2","5","8"],
	["3","6","9"]
];

function setPlayerNames() {
	var crossesPlayerName = $('#crossesPlayerName').val();
	var circlesPlayerName = $('#circlesPlayerName').val();

	if (crossesPlayerName === '' || circlesPlayerName === '') {
		alert('Both Players Names Must Be Set');

		readyTheBoard(0);
	} else {
		// push names to their places
		$('.nameOne').text(crossesPlayerName);
		$('.nameTwo').text(circlesPlayerName);

		pickRandomFirstPlayer();
		readyTheBoard(1);
	}
}

function readyTheBoard(state) {
	if (state === 1) {
		// ready the board

		// hide namesForm
		$('.namesForm').fadeOut();
		$('.instructions').fadeOut();

		// show board
		$('.boardContain').fadeIn();

	} else {
		// reset the board
		$('.tttBoard div').removeClass('taken playerOne playerTwo').html('');
		$('p.instructions').html('').hide();

		// reset labels
		$('.nameOne, .nameTwo').removeClass('label label-success');

		// reset global vars
		playerOneScoreArray = [],
		playerTwoScoreArray = [];

		pickRandomFirstPlayer();
	}
}

function pickRandomFirstPlayer() {
	playerOneName = $('.nameOne').text();
	playerTwoName = $('.nameTwo').text();

	var	startingPlayer,
	startingPlayerIcon;

	var randomNum = getRandomInt(0, 1);

	if (randomNum > 0) {
		// playerOne goes first
		startingPlayer = playerOneName;
		startingPlayerIcon = playerOneIconHtml;
		$('.nameOneContain').addClass('label label-success');
		currentPlayer = 'playerOne';
	} else {
		// playerTwo goes first
		startingPlayer = playerTwoName;
		startingPlayerIcon = playerTwoIconHtml;
		$('.nameTwoContain').addClass('label label-success');
		currentPlayer = 'playerTwo';
	}

	// set current player values
	$('.currentPlayer').text(startingPlayer);
	$('.currentTurnIcon').html(startingPlayerIcon);
}

function setCurrentPlayerMove(thisElement) {
	// test to make sure this spot is valid before doing anything else
	if ($(thisElement).hasClass('taken')) {
		console.log(thisElement + 'is already taken');
		return '';
	} else {
		// not taken
		// add classes and push icon
		$(thisElement).addClass('taken ' + currentPlayer).html(getCurrentUserIcon());

		// add this element id to player score array
		addThisMoveToScoreArray($(thisElement).attr('id'));

		// test score to determine if player has won
		testScore();

		// switch player and end turn
		switchPlayer();
	}
}

function getCurrentUserIcon() {
	// goal is to set global var currentPlayerIcon
	// and return correct user icon html
	if (currentPlayer === 'playerOne') {
		currentPlayerIcon = playerOneIconHtml;
		return currentPlayerIcon;
	} else {
		currentPlayerIcon = playerTwoIconHtml;
		return currentPlayerIcon;
	}
}

function addThisMoveToScoreArray(boxId) {
	if (currentPlayer === 'playerOne') {
		playerOneScoreArray.push(boxId);
	} else {
		playerTwoScoreArray.push(boxId);
	}
}

function switchPlayer() {
	if (currentPlayer === 'playerOne') {
		currentPlayer = 'playerTwo';
		$('.nameTwoContain').addClass('label label-success');
		$('.nameOneContain').removeClass('label label-success');
		$('.currentPlayer').text(playerTwoName);
		$('.currentTurnIcon').html(playerTwoIconHtml);
	} else {
		currentPlayer = 'playerOne';
		$('.nameOneContain').addClass('label label-success');
		$('.nameTwoContain').removeClass('label label-success');
		$('.currentPlayer').text(playerOneName);
		$('.currentTurnIcon').html(playerOneIconHtml);
	}
}

function testScore() {
	var arrayToScore,
	hasWon = 0;

	if (currentPlayer === 'playerOne') {
		// playerOneScoreArray
		arrayToScore = playerOneScoreArray;
	} else {
		// playerTwoScoreArray
		arrayToScore = playerTwoScoreArray;
	}

	/* Test for winning move:
	 *  iterate through multi demensional array to detect if use has a wining move
	 *  first loop iterates through winning combo
	 */

	for (var i = 0; i < winningMoves.length; i++) {
		// second loop iterates through each individual score
		// to determine if the uses score array contains any wining moves
		var matchesFound = 0;

		for (var j = 0; j < winningMoves[i].length; j++) {

			// Debug
			// console.log('inArray : ' + $.inArray(winningMoves[i][j], arrayToScore));

			if ($.inArray(winningMoves[i][j], arrayToScore) !== -1) {
				matchesFound++;
			} else {
				matchesFound = 0;
			}

			if (matchesFound === 3 && hasWon === 0) {
				console.log('user ' + currentPlayer + ' has a winning move');

				playerWins(currentPlayer);

				// flag to prevent from fireing more than once
				hasWon = 1;
			}
		}

		// DEBUG
		/*
		console.log('currentPlayer : ' + currentPlayer);
		console.log('arrayToScore : ' + arrayToScore);
		console.log('arrayToMatch : ' + winningMoves[i]);
		console.log('matchesFound : ' + matchesFound);
		console.log('---------------------------------');
		*/

	} // closes first loop & test for winning move

	// Test for tie game
	if ($('.tttBoard div.taken').length === 9) {
		tieGame();
	}
}

function playerWins(thisPlayer) {
	var winningPlayerName,
	winningPlayerIcon;

	if (thisPlayer ===  'playerOne') {
		// set vars for playerOne
		winningPlayerName = $('.nameOne').text();
		winningPlayerIcon = playerOneIconHtml;
	} else {
		// set vars for playerTwo
		winningPlayerName = $('.nameTwo').text();
		winningPlayerIcon = playerTwoIconHtml;
	}

	var modalHeaderHtml = winningPlayerIcon + ' ' + winningPlayerName + ' has Won!';
	var modalBodyHtml = '<p><b>Good Game!</b> <br/>Pick an option to play again</p>';
	var modalFooterHtml = '<div class="btn resetBtn">Reset</div><div class="btn btn-primary rematchBtn">Rematch</div>';

	// inject into modal
	$('.modal #modalHeader').html(modalHeaderHtml);
	$('.modal .modal-body').html(modalBodyHtml);
	$('.modal .modal-footer').html(modalFooterHtml);

	// fire modal
	$('.modal').modal('show');

	// push buttons below h1 incase user closes modal
	$('p.instructions').show().html(modalFooterHtml);

}

function tieGame() {
	var modalHeaderHtml = 'Tie Game';
	var modalBodyHtml = '<p>Pick an option to play again</p>';
	var modalFooterHtml = '<div class="btn resetBtn">Reset</div><div class="btn btn-primary rematchBtn">Rematch</div>';

	// inject into modal
	$('.modal #modalHeader').html(modalHeaderHtml);
	$('.modal .modal-body').html(modalBodyHtml);
	$('.modal .modal-footer').html(modalFooterHtml);

	// fire modal
	$('.modal').modal('show');

	// push buttons below h1 incase user closes modal
	$('p.instructions').show().html(modalFooterHtml);
}

function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}